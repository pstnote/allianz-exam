# Allianz Exam - Employee Service

## Installation

1. Clone Project

```bash
git clone git@gitlab.com:pstnote/allianz-exam.git
```
2. Install Dependencies

## Run

1. Go to project
2. In packet "com.allianz.employee", Run "AllianzExamApplication" class

## Run(Docker)

1. Go to project
2. In Terminal, Run command
```bash
mvn clean package
docker build -t allianz-exam .
docker run -p 8080:8080 allianz-exam
```

## Authorization
Generate token 
```
http://localhost:8080/employee/login
Username: admin
Password: password
```
## Swagger UI
```
http://localhost:8080/employee/swagger-ui.html
```

## H2 Database
```
Console: http://localhost:8080/employee/h2-console
JDBC Url: jdbc:h2:mem:employeedb
Username: sa
Password: password
```