package com.allianz.employee.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeDTO implements Serializable {

    private static final long serialVersionUID = -5031389896912253301L;
    private Integer id;

    @NotBlank(message = "Firstname is mandatory")
    private String firstName;

    @NotBlank(message = "LastName is mandatory")
    private String lastName;
    private String position;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;

}