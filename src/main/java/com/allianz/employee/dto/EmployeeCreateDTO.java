package com.allianz.employee.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Data
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeCreateDTO implements Serializable {

    private static final long serialVersionUID = -6301832985236590579L;
    @NotBlank(message = "Firstname is mandatory")
    private String firstName;

    @NotBlank(message = "LastName is mandatory")
    private String lastName;
    private String position;

}