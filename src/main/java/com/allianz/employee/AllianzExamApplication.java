package com.allianz.employee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AllianzExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(AllianzExamApplication.class, args);
	}

}
