package com.allianz.employee.controller.api;

import com.allianz.employee.dto.EmployeeCreateDTO;
import com.allianz.employee.dto.EmployeeDTO;
import com.allianz.employee.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Employee Controller
 * Create REST API endpoint for employee service
 */
@Api
@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    @ApiOperation(value = "Retrieve all employee")
    public ResponseEntity<List<EmployeeDTO>> retrieveAll() {
        return ResponseEntity.ok(employeeService.retrieveAll());
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Retrieve employee by Id")
    public ResponseEntity<EmployeeDTO> retrieveById(@PathVariable int id) {
        return ResponseEntity.ok(employeeService.retrieveById(id));
    }

    @PostMapping
    @ApiOperation(value = "Save employee")
    public ResponseEntity<EmployeeDTO> save(@Valid @RequestBody EmployeeCreateDTO employee) {
        return ResponseEntity.ok(employeeService.saveEmployee(employee));
    }

    @PutMapping
    @ApiOperation(value = "Update employee")
    public ResponseEntity<EmployeeDTO> update(@RequestBody EmployeeDTO employee) {
        return ResponseEntity.ok(employeeService.updateEmployee(employee));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete employee")
    public ResponseEntity delete(@PathVariable int id) {
        employeeService.deleteEmployee(id);
        return ResponseEntity.noContent().build();
    }


    /**
     * This method used to handle error response in this controller
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}