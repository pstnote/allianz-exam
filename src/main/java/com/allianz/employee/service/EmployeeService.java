package com.allianz.employee.service;

import com.allianz.employee.dto.EmployeeCreateDTO;
import com.allianz.employee.dto.EmployeeDTO;
import com.allianz.employee.entity.EmployeeEntity;
import com.allianz.employee.exception.EmployeeNotFoundException;
import com.allianz.employee.repository.EmployeeRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Employee Service
 *
 */
@Service
public class EmployeeService {

    @Autowired
    private ModelMapper modelMapper; // helper for object mapping

    @Autowired
    private EmployeeRepository employeeRepository;

    public List<EmployeeDTO> retrieveAll() {
        List<EmployeeEntity> list = employeeRepository.findAll();
        return modelMapper.map(list,  new TypeToken<List<EmployeeDTO>>() {}.getType());
    }

    public EmployeeDTO retrieveById(int id) {
        Optional<EmployeeEntity> optional = employeeRepository.findById(id);
        if (!optional.isPresent()) {
            throw new EmployeeNotFoundException(); // return 404 if not found data
        }
        return modelMapper.map(optional.get(), EmployeeDTO.class);
    }

    public EmployeeDTO saveEmployee(EmployeeCreateDTO employee) {
        EmployeeEntity employeeEntity = modelMapper.map(employee, EmployeeEntity.class);
        employeeEntity.setCreatedBy("system");
        employeeEntity.setCreatedDate(new Date());
        employeeEntity = employeeRepository.save(employeeEntity);
        return modelMapper.map(employeeEntity, EmployeeDTO.class);
    }

    public EmployeeDTO updateEmployee(EmployeeDTO employee) {
        EmployeeEntity employeeEntity = modelMapper.map(employee, EmployeeEntity.class);
        employeeEntity.setUpdatedBy("system");
        employeeEntity.setUpdatedDate(new Date());
        employeeRepository.save(employeeEntity);
        return modelMapper.map(employeeEntity, EmployeeDTO.class);
    }

    public void deleteEmployee(int id) {
        employeeRepository.deleteById(id);
    }
}
