-- This script used to initial data for H2 database
DROP TABLE IF EXISTS employees;

CREATE TABLE employees (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  position VARCHAR(250) DEFAULT NULL,
  created_by VARCHAR(250),
  created_date TIMESTAMP,
  updated_by VARCHAR(250),
  updated_date TIMESTAMP
);

INSERT INTO employees (first_name, last_name, position, created_by, created_date) VALUES
  ('Phongsathon', 'Suriyo', 'Backend Developer', 'system', CURRENT_TIMESTAMP);