package com.allianz.employee.service;

import com.allianz.employee.entity.EmployeeEntity;
import com.allianz.employee.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Employee Service Test
 *
 */
@Service
@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @Test
    // Test for retrieve all
    public void retrieveAll() {
        // mock result
        List<EmployeeEntity> list = new ArrayList<>();
        EmployeeEntity entity = new EmployeeEntity();
        entity.setId(1);
        list.add(entity);
        Mockito.when(employeeRepository.findAll()).thenReturn(list);
        // if not null
        Assertions.assertNotNull(employeeRepository.findAll());
    }

    @Test
    // Test retrieve by ID
    public void retrieveById() {
        // mock result
        EmployeeEntity entity = new EmployeeEntity();
        entity.setId(1);
        entity.setFirstName("Phongsathon");
        entity.setLastName("Suriyo");
        Optional<EmployeeEntity> optional = Optional.of(entity);
        Mockito.when(employeeRepository.findById(1)).thenReturn(optional);
        // if not null
        Assertions.assertNotNull(employeeRepository.findById(1).isPresent());

    }

    // Other test case...
}
