#Base image for runtime
FROM tomcat:latest

#Copy war file into webapps folder inside Docker
ADD target/employee.war /usr/local/tomcat/webapps/

#Expose port 8080
EXPOSE 8080

#command to be executed
CMD ["catalina.sh", "run"]